package com.mrl.recon.init;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.mrl.recon.utils.PropertiesReader;

public class DBConnectionRECON {
	final static Logger logger = Logger.getLogger(DBConnectionRECON.class);

	private Connection connection;

	public Connection getConnection(int inFlag, String dbName) {
		try {
			if (StringUtils.isNotBlank(dbName)) {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				if ("MRL".equalsIgnoreCase(dbName)) {
					if ("UAT".equalsIgnoreCase(new PropertiesReader().getPropValues("profile.region"))) {
						connection = DriverManager.getConnection(
								"jdbc:sqlserver://192.168.1.60;user=riskanalyst1;password=pos@123UKS;database=AnalyticaIV_Test");
					}
					if ("PROD".equalsIgnoreCase(new PropertiesReader().getPropValues("profile.region"))) {
						connection = DriverManager.getConnection(
								"jdbc:sqlserver://192.168.1.62;user=riskanalyst1;password=pos@123UKS;database=AnalyticaIV_Test");
					}
				} else if ("SBI".equalsIgnoreCase(dbName)) {
					connection = DriverManager.getConnection(
							"jdbc:sqlserver://192.168.1.60;user=riskanalyst1;password=pos@123UKS;database=SBI_Analytica_4");
				} else if ("RML".equalsIgnoreCase(dbName)) {
					if ("UAT".equalsIgnoreCase(new PropertiesReader().getPropValues("profile.region"))) {
						connection = DriverManager.getConnection(
								"jdbc:sqlserver://192.168.1.60;user=riskanalyst1;password=pos@123UKS;database=RML_ANALYTICA_4");
					}
					if ("PROD".equalsIgnoreCase(new PropertiesReader().getPropValues("profile.region"))) {
						connection = DriverManager.getConnection(
								"jdbc:sqlserver://192.168.1.62;user=riskanalyst1;password=pos@123UKS;database=RML_ANALYTICA_4");
					}
				}
			} else {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				if (inFlag == 1) {
					connection = DriverManager.getConnection(
							"jdbc:sqlserver://192.168.215.107;user=Merchantapp;password=Merapp#@;database=MRLPOSNET_MSCRM");
				} else if (inFlag == 2) {
					connection = DriverManager.getConnection(
							"jdbc:sqlserver://192.168.1.55;user=gokulram;password=pos@123GR;database=MRLPOSNET_MSCRM");
				} else if (inFlag == 3) {
					if ("UAT".equalsIgnoreCase(new PropertiesReader().getPropValues("profile.region"))) {
						connection = DriverManager.getConnection(
								"jdbc:sqlserver://192.168.1.60;user=riskanalyst1;password=pos@123UKS;database=AnalyticaIV_Test");
					}
					if ("PROD".equalsIgnoreCase(new PropertiesReader().getPropValues("profile.region"))) {
						connection = DriverManager.getConnection(
								"jdbc:sqlserver://192.168.1.62;user=riskanalyst1;password=pos@123UKS;database=AnalyticaIV_Test");
					}
				} else if (inFlag == 4) {
					connection = DriverManager.getConnection(
							"jdbc:sqlserver://192.168.1.70;user=RiskAnalytst1;password=pos@123UKS;database=SBI_Analytica_4");
				} else if (inFlag == 5) {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/tx", "root", "developer");
				} else if (inFlag == 6) {
					connection = DriverManager.getConnection(
							"jdbc:sqlserver://192.168.215.107;user=Merchantapp;password=Merapp#@;database=testing");
				} else if (inFlag == 7) {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection("jdbc:mysql://192.168.1.44:3306/lms100", "mtapi",
							"mtapi@lms100");
				} else if (inFlag == 8) {
					connection = DriverManager.getConnection(
							"jdbc:sqlserver://192.168.1.65:15333;user=LiveTran;password=pos@Liv@;database=testingdb");
				} else if (inFlag == 9) {
					if ("UAT".equalsIgnoreCase(new PropertiesReader().getPropValues("profile.region"))) {
						connection = DriverManager.getConnection(
								"jdbc:sqlserver://192.168.1.60;user=analytica;password=pos@123IM;database=AnalyticaIV_Test");
					}
					if ("PROD".equalsIgnoreCase(new PropertiesReader().getPropValues("profile.region"))) {
						connection = DriverManager.getConnection(
								"jdbc:sqlserver://192.168.1.62;user=analytica;password=pos@123IM;database=AnalyticaIV_Test");
					}
				} else if (inFlag == 10) {
					connection = DriverManager.getConnection(
							"jdbc:sqlserver://192.168.1.65:15333;user=LiveTran;password=pos@Liv@;database=MRLPOSNET_MSCRM");
				} else if (inFlag == 11) {
					Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
					connection = DriverManager.getConnection(
							"jdbc:sqlserver://192.168.1.65:15333;user=Merchantapp;password=Merapp#@;database=MRLPOSNET_MSCRM");
				} else if (inFlag == 13) {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection("jdbc:mysql://192.168.1.44:3306/lms100", "lms100", "lms100");
				}
			}
		} catch (Exception e) {
			logger.error("Error getting in DB Connection :" + e);
		}
		return connection;
	}

	public void closeConnection(Connection connection) {
		try {
			if (connection != null && !connection.isClosed())
				connection.close();
		} catch (SQLException e) {
			logger.error("Error getting in DB Connection Close :" + e);
		}
	}
}
