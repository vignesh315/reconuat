	package com.mrl.recon.service.impl;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;

import com.mrl.recon.dao.ReconDao;
import com.mrl.recon.model.Status;
import com.mrl.recon.service.ReconService;

@ManagedBean
public class ReconServiceImpl implements ReconService {
	@Autowired
	private ReconDao reconDao ; 
	@Override
	public Status reconWelcome() {
		return reconDao.reconWelcome();
	}

}
