package com.mrl.recon.utils;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesReader {
	final static Logger logger = Logger.getLogger(PropertiesReader.class);
	InputStream inputStream;

	public String getPropValues(String key) {
		String result = "";

		try {
			Properties prop = new Properties();
			String propFileName = "hardcode-testing.properties";

			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}

			// get the property value and print it out
			return prop.getProperty(key);

		} catch (Exception e) {
			logger.error("getPropValues method key=" + key, e);
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (Exception e) {
				logger.error("getPropValues method key=" + key, e);
			}
		}
		return result;
	}
}